  import java.time.LocalDate;
  import java.util.Scanner;

  public class ATM{

    private int id;
    private double balance;
    private double tasaDeInteresAnual;
    private  LocalDate fechaDeCreacion;

    public ATM() {
      this.id = 0;
      this.balance = 0;
      this.tasaDeInteresAnual = 0;
      this.fechaDeCreacion = LocalDate.now();
    }

    public ATM(int id, double balance, double tasaDeInteresAnual) {
      this.id = id;
      this.balance = balance;
      this.tasaDeInteresAnual = tasaDeInteresAnual;
      this.fechaDeCreacion = LocalDate.now();
    }

  public double getretirarDinero(double montoretirar, double balance) {
    this.balance = balance - montoretirar;
    return this.balance;
  }

  public double getdepositarDinero (double montodepositar, double balance) {
    this.balance = balance + montodepositar;
    return this.balance;
  }

  public LocalDate getfechaDeCreacion() {
    return this.fechaDeCreacion;
  }
}