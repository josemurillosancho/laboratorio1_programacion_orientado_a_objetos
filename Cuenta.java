  import java.time.LocalDate;
  import java.util.Scanner;

  public class Cuenta{

     int id;
     double balance;
     double tasaDeInteresAnual;
     double montoretirar;
     double montodepositar;
     LocalDate fechaDeCreacion;

    public Cuenta() {
      this.id = 0;
      this.balance = 0;
      this.tasaDeInteresAnual = 0;
      this.fechaDeCreacion = LocalDate.now();
    }

    public Cuenta(int id, double balance, double tasaDeInteresAnual) {
      this.id = id;
      this.balance = balance;
      this.tasaDeInteresAnual = tasaDeInteresAnual;
      this.fechaDeCreacion = LocalDate.now();
    }

  public double getretirarDinero(double montoretirar, double balance) {
    this.balance = balance - montoretirar;
    return this.balance;
  }

  public double getdepositarDinero (double montodepositar, double balance) {
    this.balance = balance + montodepositar;
    return this.balance;
  }

  public LocalDate getfechaDeCreacion() {
    return this.fechaDeCreacion;
  }
}